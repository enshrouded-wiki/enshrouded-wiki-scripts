# World Coordinates

The world of Embervale is defined as a grid of voxels uniquely located at world coordinates **(x, y, z)**. The coordinates range from the depth of the south west corner at **(0, 0, 0)** to the peak of the north west corner at **(width, height, altitude)**. To accurately locate points of interest on the map, point coordinates **(x, y)** reference the true world coordinates in-game.

## Convert from image coordinates

To use world coordinates in maps of Embervale, we must transform the image coordinates of the map to the world coordinates of the game. From an aerial view, an orthogonal projection of the world coordinates $(x_w, y_w)$ onto image coordinates $(x_m, y_m)$ is equivalent to an affine transformation parameterized by scaling $s$ and offset $(x_o, y_o)$.

$$
\begin{align}
x_w &= s x_m + x_o \\
y_w &= s y_m + y_o
\end{align}
$$

To estimate the affine transformation parameters $(s, x_o, y_o)$ that minimizes projection error, we require at least two *control points* with accurate measurements for both their world coordinates and corresponding image coordinates.

## Standard control points

To rectify all map images of Embervale with consistent world coordinates, we standardize the control points of two farms as world coordinates $(x_w, y_w)$:

- *Rose Shell Burrow* and
- *Sunsimmer Souterrain*.

Both farms are symmetrically constructed in rectangular arrangements about their center marker, which is both precise and convenient to measure for image coordinates $(x_m, y_m)$ on the map.

| Location | Xw | Yw |
| --- | --- | --- |
| Sunsimmer Souterrain | 6415.0 | 1993.0 |
| Rose Shell Burrow | 8343.5 | 3140.5 |

## Estimate transformation parameters

Given the provided world coordinates $(x_w, y_w)$ and measured image coordinates $(x_m, y_m)$ of the standard control points, we choose the affine transformation parameters $(s, x_o, y_o)$ that best match their world coordinates to corresponding image coordinates.

The following example is a script in Julia to estimate parameters using the Moore-Penrose Pseudoinverse method. The script should be similar in Matlab or Octave.

```julia
# Sunsimmer Souterrain
Xw1, Yw1 = 6415.0, 1993.0 # Standard world coordinates.
Xm1, Ym1 = 6018.0, 1336.0 # Measured image coordinates.

# Rose Shell Burrow
Xw2, Yw2 = 8343.5, 3140.5 # Standard world coordinates.
Xm2, Ym2 = 7382.0, 2149.0 # Measured image coordinates.

# Estimate transformation parameters
A = [ Xw1 1 0 ;
      Yw1 0 1 ;
      Xw2 1 0 ;
      Yw2 0 1 ]
b = [ Xm1, Ym1, Xm2, Ym2 ]
params = A \ b

s  = params[1]
xo = params[2]
yo = params[3]
```

Since the control points are standardized, we do not prescribe a standard method for parameter estimation.